package com.artdan.cloud.artdancloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class ArtDanCloudApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArtDanCloudApplication.class, args);
	}
}
